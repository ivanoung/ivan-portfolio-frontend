export interface IProjectGridProps {
  projects: IProject[];
  isPageExpanded: boolean;
}

export interface IProject {
  name: string;
  brief: string;
  imageLoc: any;
  isFeature: boolean;
  catTag: string;
  linkText?: string;
  linkURL?: string;
}

export interface IHomepageStates {
  isPageExpanded: boolean;
}
