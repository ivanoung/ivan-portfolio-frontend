// Importing modules
import * as React from "react";

// Importing styling and static assets
import { data } from "src/Contents/contentData";
import "./Homepage.scss";

// Importing reduc and friends
// Importing react routering

// Importing UI elements
import AboutMe from "src/Components/AboutMe/AboutMe";
import CallToAction from "src/Components/CallToAction/CallToAction";
import ExpandButton from "src/Components/ExpandButton/ExpandButton";
import Footer from "src/Components/Footer/Footer";
import HeroSection from "src/Components/HeroSection/HeroSection";
import CreationIntro from "src/Components/Intros/CreationIntro";
import ExposureIntro from "src/Components/Intros/ExposureIntro";
import OutsideWorkIntro from "src/Components/Intros/OutsideWorkIntro";
import NavBar from "src/Components/NavBar/NavBar";
import ProjectGrid from "src/Components/ProjectGrid/ProjectGrid";
import SectionDivider from "src/Components/SectionDivider/SectionDivider";

// Importing interfaces
import { IHomepageStates } from "src/Interfaces/Interfaces";

export default class Homepage extends React.Component<{}, IHomepageStates> {
  constructor(props: {}) {
    super(props);
    this.state = { isPageExpanded: false };
    this.expand = this.expand.bind(this);
  }

  public expand = () => {
    this.setState({ isPageExpanded: true });
  };

  public render() {
    return (
      <body className="fullWidth pageContainer">
        <NavBar />
        <HeroSection />
        {/* About me */}
        <SectionDivider />
        <AboutMe />
        <CallToAction />
        {/* Exposure */}
        <SectionDivider />
        <ExposureIntro />
        <ProjectGrid
          projects={...data.exposures}
          isPageExpanded={this.state.isPageExpanded}
        />
        {/* Creation */}
        <SectionDivider />
        <CreationIntro />
        <ProjectGrid
          projects={...data.creations}
          isPageExpanded={this.state.isPageExpanded}
        />
        {/* Outside work */}
        <SectionDivider />
        <OutsideWorkIntro />
        <ProjectGrid
          projects={...data.outsideWork}
          isPageExpanded={this.state.isPageExpanded}
        />
        {/* Company Grey box */}
        {!this.state.isPageExpanded && <ExpandButton activate={this.expand} />}
        <Footer />
      </body>
    );
  }
}
