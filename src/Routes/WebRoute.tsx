// Importing modules
import * as React from "react";
import { Route, Switch } from "react-router-dom";

// Importing styling and static assets
import Homepage from "src/Pages/Homepage/Homepage";
// Importing reduc and friends
// Importing react routering
// Importing interfaces

export default class WebRoute extends React.Component {
  public render() {
    return (
      <Switch>
        <Route path="/" component={Homepage} />
      </Switch>
    );
  }
}
