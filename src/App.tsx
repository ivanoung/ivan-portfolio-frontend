import * as React from "react";
import "./App.scss";

import WebRoute from "src/Routes/WebRoute";

class App extends React.Component {
  public render() {
    return <WebRoute/>;
  }
}

export default App;
