// Exposures
import aim from "./Images/aim.jpg";
import discovery from "./Images/discovery.jpg";
import ipsos from "./Images/ipsos.jpg";
import unesco from "./Images/unesco.jpg";

// Creations
import cashGun from "./Images/cashGun.jpg";
import dealingRoom from "./Images/dealingRoom.jpg";
import milk from "./Images/milk.jpg";

// Outside Work
import centreO from "./Images/centreO.jpg";
import kidsCanCode from "./Images/kidsCanCode.jpg";
import itHeyDay from "./Images/ltHeyDay.jpg";
import oriental from "./Images/oriental.jpg";
import stgss from "./Images/stgss.jpg";
import swMentor from "./Images/swMentor.jpg";
import swValid from "./Images/swValid.jpg";

export const data = {
  exposures: [
    {
      name: "ODLTG",
      brief:
        "A collaboration between UNESCO and Tencent, aim to preserve hstorical, traditional games in the form of digital cloud.",
      imageLoc: unesco,
      isFeature: true,
      catTag: "Translation",
      linkText: "Publicaiton",
      linkURL:
        "https://medium.com/itencent/2016-annual-conference-meeting-6bb5fea5372b"
    },
    {
      name: "Co-creator Invitation",
      brief:
        "While taking hard look into what E-sports and gaming is nowadays, Tencent and Discovery collaborates on a documentary video.",
      imageLoc: discovery,
      isFeature: false,
      catTag: "Social Media",
      linkText: "Medium Invitation",
      linkURL:
        "https://medium.com/itencent/discovery-channel-co-producer-invitation-2e94a0b138da"
    },
    {
      name: "IPSOS Canada",
      brief:
        "Marketing research and survey is the heart of any marketing campaign, to pay attention to what the customers reall need.",
      imageLoc: ipsos,
      isFeature: false,
      catTag: "Translation"
      // linkText: "Case Study",
      // linkURL: "https://www.ivanoung.io"
    },
    {
      name: "Social Media Tool Map",
      brief:
        "A map of tools in the field of digital marketing and design, created for a local digital studio to improve their productivity and profitablity.",
      imageLoc: aim,
      isFeature: true,
      catTag: "Business Consultation",
      linkText: "Final Production",
      linkURL:
        "https://drive.google.com/drive/folders/1dIzp7BXrkM7fhr6UfmHid1u1DfklEOV9"
    }
  ],
  creations: [
    {
      name: "Dealing Room",
      brief:
        "A web-based POS to your bar/restaurant for your slow moving stock and lack of upsale opportunities problem.",
      imageLoc: dealingRoom,
      isFeature: true,
      catTag: "Product Management",
      linkText: "Case Study",
      linkURL:
        "https://uxdesign.cc/product-development-case-study-from-ideation-to-pitching-with-product-ff1aaa90aefe"
    },
    {
      name: "Milk Turn Bad",
      brief:
        "The milk turned bad, and very naughty too. Creativity isn’t a myth, but a different way of looking at existing ideas.",
      imageLoc: milk,
      isFeature: false,
      catTag: "Exploratory",
      linkText: "Image Gallery",
      linkURL:
        "https://www.behance.net/gallery/72826725/8-Bit-Punny-T-shirt-design"
    },
    {
      name: "Re:Cash Gun",
      brief:
        "You don’t have to be rich to be balling. A prototype of the “cash gun”, built solely out of laser-cut plywood and acrylics.",
      imageLoc: cashGun,
      isFeature: false,
      catTag: "Exploratory"
      // linkText: "Case Study",
      // linkURL: "https://www.ivanoung.io"
    }
  ],
  outsideWork: [
    {
      name: "Technical Mentor",
      brief:
        "Collaboration of Mandarin Oriental Hong Kong and Startup Weekend, to come up with innovative solutions for hospitality challenges.",
      imageLoc: oriental,
      isFeature: false,
      catTag: "Mentorship"
      // linkText: "Case Study",
      // linkURL: "https://www.ivanoung.io"
    },
    {
      name: "Validation Award",
      brief:
        "There is no better way of doing good business better than understand how to build one. Get your hands dirty, start smart, and profit.",
      imageLoc: swValid,
      isFeature: false,
      catTag: "Public Speaking"
      // linkText: "Case Study",
      // linkURL: "https://www.ivanoung.io"
    },
    {
      name: "Startup Weekend Mentor",
      brief:
        "To give back to the society whenever opportunities present itself. Starting with an idea ain’t hard after all, but executing it is.",
      imageLoc: swMentor,
      isFeature: false,
      catTag: "Mentorship"
      // linkText: "Case Study",
      // linkURL: "https://www.ivanoung.io"
    },
    {
      name: "Grinding In Startups",
      brief:
        "An invitation back to my secondary school to share some of my experiences and exposures in the field of startup and running business.",
      imageLoc: stgss,
      isFeature: false,
      catTag: "Mentorship"
      // linkText: "Case Study",
      // linkURL: "https://www.ivanoung.io"
    },
    {
      name: "Digital Media Outside China",
      brief:
        "A 2-hour workshop for LT Heyday, sharing tools that I use and approaches towards the discipline of digital marketing outside China.",
      imageLoc: itHeyDay,
      isFeature: true,
      catTag: "Social Media",
      linkText: "Presentation Stack",
      linkURL:
        "https://docs.google.com/presentation/d/1WtU_HHJlI36TOS-BmKc0YQbguurqIP4rjTfHjHu-LGc/edit?usp=sharing"
    },
    {
      name: "Kids Can Code",
      brief:
        "A 3-day workshop to teach kids from primary school to secondary school how to program in Python using Raspberry Pi.",
      imageLoc: kidsCanCode,
      isFeature: false,
      catTag: "Public Speaking"
      // linkText: "Case Study",
      // linkURL: "https://www.ivanoung.io"
    },
    {
      name: "Digital Marketing 101",
      brief:
        "A 1-hour workshop with games, picture what digital marketing really is without all the jargons and confusing hypes from the industry.",
      imageLoc: centreO,
      isFeature: false,
      catTag: "Project Management"
      // linkText: "Case Study",
      // linkURL: "https://www.ivanoung.io"
    }
    // ,
    // {
    //   name: "Design Thinking Worksop",
    //   brief: "DT Workshop with kids",
    //   imageLoc: centreO,
    //   isFeature: false,
    //   catTag: "Public Speaking",
    //   linkText: "Case Study",
    //   linkURL: "https://www.ivanoung.io"
    // }
  ]
};
