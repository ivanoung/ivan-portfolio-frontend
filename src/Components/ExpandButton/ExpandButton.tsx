// Importing modules
import * as React from "react";

// Importing styling and static assets
import "./ExpandButton.scss";

// Importing reduc and friends
// Importing react routering
// Importing UI elements
// Importing interfaces

interface IExpandButtonProps {
  activate: () => void;
}

export default class ExpandButton extends React.Component<IExpandButtonProps> {
  constructor(props: any) {
    super(props);
  }

  public render() {
    return (
      <button
        className="expandBtn rounded fullWidth"
        onClick={this.props.activate}
      >
        <h3>Click to see more projects</h3>
      </button>
    );
  }
}
