// Importing modules
import * as React from "react";

// Importing styling and static assets
import "./CompanyGrid.scss";
import c6p from "./Images/c6p.png";
import centreO from "./Images/centreO.png";
import ipsos from "./Images/ipsos.png";
import heyDay from "./Images/lt.png";
import stgss from "./Images/stgss.png";
import sw from "./Images/sw.png";
import tencent from "./Images/tencent.png";
import unesco from "./Images/unesco.png";

// Importing reduc and friends
// Importing react routering
// Importing UI elements
// Importing interfaces

export default class CompanyGrid extends React.Component<{}> {
  constructor(props: any) {
    super(props);
  }

  public render() {
    return (
      <div className="rounded" id="projectGrid">
        <img className="fullWidth projectImg" src={unesco} alt="UNESCO Logo" />
        <img
          className="fullWidth projectImg"
          src={tencent}
          alt="Tencente Entertainment Logo"
        />
        <img
          className="fullWidth projectImg"
          src={sw}
          alt="Startup Weekend Logo"
        />
        <img className="fullWidth projectImg" src={ipsos} alt="IPSOS Logo" />
        <img className="fullWidth projectImg" src={stgss} alt="STGSS Logo" />
        <img
          className="fullWidth projectImg"
          src={heyDay}
          alt="LT Hey Day Logo"
        />
        <img
          className="fullWidth projectImg"
          src={centreO}
          alt="Centre O Logo"
        />
        <img
          className="fullWidth projectImg"
          src={c6p}
          alt="Cohort 6 Productions Logo"
        />
      </div>
    );
  }
}
