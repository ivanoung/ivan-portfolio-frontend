// Importing modules
import * as React from "react";

// Importing styling and static assets
import envir from "./Images/envelope.svg";
import logo from "./Images/logo@2x.png";
import "./NavBar.scss";
// Importing reduc and friends
// Importing react routering
// Importing UI elements
// Importing interfaces

export default class NavBar extends React.Component<{}> {
  constructor(props: any) {
    super(props);
  }

  public render() {
    return (
      <div className="fullWidth nav">
        <a className="nav__cont"href="">
          <img className="nav__cont__leftLogo" src={logo} alt="" />
        </a>
        <a className="nav__cont"href="mailto:ivanoung@gmail.com">
          <img className="nav__cont__rightLogo" src={envir} alt="" />
        </a>
      </div>
    );
  }
}
