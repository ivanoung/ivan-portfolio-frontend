// Importing modules
import * as React from "react";

// Importing styling and static assets
import "./HeroSection.scss";
// Importing reduc and friends
// Importing react routering
// Importing UI elements
// Importing interfaces

export default class HeroSection extends React.Component<{}> {
  constructor(props: any) {
    super(props);
  }

  public render() {
    return (
      <header className="fullWidth hero">
        <h1 className="text-blue">
          Ivanoung.io is the online home of Ivan Oung - Fullstack Developer,
          Digital Marketer, Perpetual Entrepreneur, and{" "}
          <h1 className="text-blue focus">Sprint Facilitator</h1>.
        </h1>
      </header>
    );
  }
}
