// Importing modules
import * as React from "react";

// Importing styling and static assets
import "./AboutMe.scss";
// Importing reduc and friends
// Importing react routering
// Importing UI elements
// Importing interfaces

export default class AboutMe extends React.Component {
  public render() {
    return (
      <div className="aboutMe fullWidth">
        <div className="aboutMe__head">
          <div className="aboutMe__head__container">
            <h2 className="aboutMe__head__container__header text-blue">
              About Me
            </h2>
            <h3 className="aboutMe__head__container__caption">
              A perpetually driven Rubik's cube with a core made of water.
              Logical, yet creatively flexible; inclusive, yet penetrative.
            </h3>
          </div>

          <div className="aboutMe__head__body">
            <p>
              I've always known that I have a knack for challenges and asking
              questions since my napping days. Lego was my best friend which got
              me through adolescent.
            </p>
            <p>
              During my years back in primary school, classes and daily routines
              were not enough to keep me engaged. Interschool choir competition
              was the only place where I feel like home.
            </p>
          </div>
        </div>
        <div className="aboutMe__body">
          <p>
            I was never satisfied with the answer "just is". Throughout the
            years after I graduated from University, my curiosity has been
            pushing me to experience and understand my own boundaries and
            interests lies. Over the last 12 years, I've worked in more than 7
            different industries, from Food and Beverages, hospitality, sales,
            translations to digital marketing and programming.
          </p>
          <p>
            If I were to say what I'm most proud of, is my thirst for
            cross-fields knowledge and how many times have I ask the question,
            "why?" Even though I did not have the luxury to receive any proper
            training on what I do now, but books and articles are always my
            favorite mentor. Answers are always there for you, the only thing
            you have to do is to spend some time to talk to them.
          </p>
          <p>
            Even my working history looks more like "Where is Waldo", but what
            I've learned from those experience allowed me to reach out to places
            and encounters that I can never imagine.
          </p>
        </div>
      </div>
    );
  }
}
