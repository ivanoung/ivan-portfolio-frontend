// Importing modules
import * as React from "react";

// Importing styling and static assets
import "./Intros.scss";
// Importing reduc and friends
// Importing react routering
// Importing UI elements
// Importing interfaces

export default class ExposureIntro extends React.Component {
  public render() {
    return (
      <div className="section">
        <div className="section__head">
          <h2 className="section__head__header text-blue">Exposures</h2>
          <h3 className="section__head__caption">
            The enjoyment of the relentless battle through the chaotic structure
            and designed mess to be known as; life.
          </h3>
        </div>
        <div className="section__body">
          <p>
            The quality of one's life depends on the complexity of their
            exposure and how that rich, complexfully-flavored experience is
            converted into knowledge. This vast world is filled with an
            immersive amount of knowledge and wisdom, beyond moldy folders and
            binders.
          </p>
          <p>
            Only those with the courage to sacrifice stability and embrace the
            wave shall be reward with the smile of Athena at the line of goal.
          </p>
        </div>
      </div>
    );
  }
}
