// Importing modules
import * as React from "react";

// Importing styling and static assets
import "./Intros.scss";
// Importing reduc and friends
// Importing react routering
// Importing UI elements
// Importing interfaces

export default class OutsideWorkIntro extends React.Component {
  public render() {
    return (
      <div className="section fullWidth">
        <div className="section__head">
          <h2 className="section__head__header text-blue">Outside Work</h2>
          <h3 className="section__head__caption">
            Money and status are not the indicators of how influential a person
            is, but how many lives were changed is.
          </h3>
        </div>
        <div className="section__body">
          <p>
            My mother has been a teacher for the last 30 few years, her students
            from her first year of teaching still remembers her. Because of
            that, I respect a lot with how she approaches teaching.
          </p>
          <p>
            Her motto on teaching is, "because I was lucky enough to receive
            education during harsh times, it only makes sense to return the
            favor by helping the society to become a better place with
            well-educated minds."
          </p>
        </div>
      </div>
    );
  }
}
