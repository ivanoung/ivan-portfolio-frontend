// Importing modules
import * as React from "react";

// Importing styling and static assets
import "./Intros.scss";
// Importing reduc and friends
// Importing react routering
// Importing UI elements
// Importing interfaces

export default class CreationIntro extends React.Component {
  public render() {
    return (
      <div className="section">
        <div className="section__head">
          <h2 className="section__head__header text-blue">Creations</h2>
          <h3 className="section__head__caption">
            "I think God, in creating man, somewhat overestimated his ability."
            - Oscar Wilde
          </h3>
        </div>
        <div className="section__body">
          <p>
            Creativity builds on the foundation of iterations and experiments. A
            clear-carved and smooth journey is more than toxic to ones' growth.
          </p>
          <p>
            The only way to find out what lies ahead is to walk through the mist
            of unknowns, to champion the truth that solely belongs to you by
            learning from your failures.
          </p>
        </div>
      </div>
    );
  }
}
