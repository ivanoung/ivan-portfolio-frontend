// Importing modules
import * as React from "react";
// Importing styling and static assets
import "./SectionDivider.scss";
// Importing reduc and friends
// Importing react routering
// Importing UI elements
// Importing interfaces

export default class SectionDivider extends React.Component {

  public render() {
    return (
      <div className="fullWidth">
        <hr className="sectionDivider" />
      </div>
    );
  }
}
