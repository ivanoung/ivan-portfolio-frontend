// Importing modules
import * as React from "react";

// Importing styling and static assets
import "./Footer.scss";

// Importing Social icons
import Beh from "./Icons/behance.svg";
import Code from "./Icons/codepen.svg";
import Mail from "./Icons/envelope.svg";
import Git from "./Icons/github.svg";
import Lin from "./Icons/linkedin.svg";
import Med from "./Icons/medium.svg";
// import Yt from "./Icons/youtube.svg";

// Importing reduc and friends
// Importing react routering
// Importing UI elements
// Importing interfaces

export default class Footer extends React.Component<{}, { thisYear: any }> {
  constructor(props: any) {
    super(props);

    // const findThisYear = () => {
    //   const year = new Date(Date.now());
    //   const yearString = year.getFullYear.toString();
    //   return yearString;
    // };

    this.state = {
      thisYear: 2018
    };
  }

  public render() {
    return (
      <footer className="fullWidth">
        <hr className="fullWidthHr" />
        <div className="footContainer">
          <div className="footContainer__social">
            <div className="footContainer__social__iconContainer">
              <a
                href="https://medium.com/@ivanoung"
                title="Ivan Oung's Medium Page"
              >
                <img
                  src={Med}
                  alt="Medium Logo"
                  className="footContainer__social__iconContainer__icons"
                />
              </a>
              <a
                href="https://www.behance.net/ivanthechaos"
                title="Ivan Oung's Behance Portfolio"
              >
                <img
                  src={Beh}
                  alt="Behance Logo"
                  className="footContainer__social__iconContainer__icons"
                />
              </a>
              <a
                href="https://github.com/ivanoung"
                title="Ivan Oung's Github Page"
              >
                <img
                  src={Git}
                  alt="Github Logo"
                  className="footContainer__social__iconContainer__icons"
                />
              </a>
              <a
                href="https://www.linkedin.com/in/ivanoung/"
                title="Ivan Oung's LinkedIn Page"
              >
                <img
                  src={Lin}
                  alt="LinkedIn Logo"
                  className="footContainer__social__iconContainer__icons"
                />
              </a>
              {/* <a href="" title="Ivan Oung's Youtube Channel">
                <img
                  src={Yt}
                  alt="Youtube Logo"
                  className="footContainer__social__iconContainer__icons"
                />
              </a> */}
              <a
                href="https://codepen.io/ivanoung/"
                title="Ivan Oung's Codepen Profile"
              >
                <img
                  src={Code}
                  alt="Codepen Logo"
                  className="footContainer__social__iconContainer__icons"
                />
              </a>
            </div>
            <div className="footContainer__social__text">
              <span>
                &#169;{this.state.thisYear} Ivan Oung. Made in Hong Kong
              </span>
            </div>
          </div>
          <div className="footContainer__home">
            <a href="mailto:ivanoung@gmail.com" title="Ivan Oung's Email">
              <img
                className="footContainer__social__iconContainer__icons"
                src={Mail}
                alt="Mail Icon"
              />
            </a>
          </div>
        </div>
      </footer>
    );
  }
}
