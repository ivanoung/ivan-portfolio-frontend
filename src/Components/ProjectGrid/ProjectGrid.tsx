// Importing modules
import * as React from "react";

// Importing styling and static assets
import "./ProjectGrid.scss";

// Fontawesome
import { faExternalLinkAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// Importing interfaces
import { IProject, IProjectGridProps } from "src/Interfaces/Interfaces";

export default class ProjectGrid extends React.Component<IProjectGridProps> {
  constructor(props: IProjectGridProps) {
    super(props);
  }
  // key, imageLoc, name, brief, isFeature, catTag, linkText, linkURL
  public box = (projectData: IProject, key: number) => (
    <li key={key} className="projectGrid__container">
      {/* Image box */}
      <div className="projectGrid__container__imgContainer rounded">
        {/* Image */}
        <img
          src={projectData.imageLoc}
          alt={`Index image of ` + projectData.name}
          className="projectGrid__container__imgContainer__img fullWidth"
        />
        {/* Box for tags */}
        <div className="projectGrid__container__imgContainer__pjTag">
          <span>{projectData.catTag}</span>
        </div>
      </div>

      {/* Project name */}
      <h3 className="projectGrid__container__header text-blue">
        {projectData.name}
      </h3>

      {/* Project descriptions */}
      <p className="projectGrid__container__body">{projectData.brief}</p>

      {/* Project external links */}
      {projectData.linkURL !== undefined && (
        <a
          href={projectData.linkURL}
          className="projectGrid__container__extLinkContainer"
        >
          <FontAwesomeIcon
            style={{ color: "#21409a" }}
            icon={faExternalLinkAlt}
          />{" "}
          <span>{projectData.linkText}</span>
        </a>
      )}
    </li>
  );

  public render() {
    return (
      <ul className="fullWidth projectGrid">
        {this.props.projects.map((project, i) => {
          return !this.props.isPageExpanded
            ? project.isFeature && this.box(project, i)
            : this.box(project, i);
        })}
      </ul>
    );
  }
}
