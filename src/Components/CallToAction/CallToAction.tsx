// Importing modules
import * as React from "react";

// Importing styling and static assets
import "./CallToAction.scss";

// Importing reduc and friends
// Importing react routering
// Importing UI elements
// Importing interfaces

export default class CallToAction extends React.Component<{}> {
  constructor(props: any) {
    super(props);
  }

  public render() {
    return (
      <section className="ctaBar rounded">
        <div className="ctaBar__left">
          <span>
            Send me an email about any ideas or opportunities and I'll get back
            to you ASAP.
          </span>
        </div>
        <div className="ctaBar__right">
          <a href="mailto:ivanoung@gmail.com">
            <button className="ctaBar__right__button rounded">Email Me</button>
          </a>
        </div>
      </section>
    );
  }
}
